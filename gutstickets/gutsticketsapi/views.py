from django.shortcuts import render
from rest_framework import viewsets
# Create your views here.
from gutsticketsapi.models import Hall
from gutsticketsapi.serializers import HallSerializer


class HallViewSet(viewsets.ModelViewSet):
    queryset = Hall.objects.all()
    serializer_class = HallSerializer
