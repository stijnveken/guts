from django.db import models


class Hall(models.Model):
    name = models.CharField(max_length=100)


class Section(models.Model):
    name = models.CharField(max_length=100)
    hall = models.ForeignKey(Hall, on_delete=models.CASCADE)


class Rank(models.Model):
    name = models.CharField(max_length=100)
    section = models.ForeignKey(Section, on_delete=models.CASCADE)


class Row(models.Model):
    number = models.IntegerField()
    rank = models.ForeignKey(Rank, on_delete=models.CASCADE)


class Seat(models.Model):
    number = models.IntegerField()
    row = models.ForeignKey(Row, on_delete=models.CASCADE)


class Show(models.Model):
    name = models.CharField(max_length=100)
    datetime = models.DateTimeField()


class Reservation(models.Model):
    name = models.CharField(max_length=100)
    show = models.ForeignKey(Show, on_delete=models.CASCADE)
    seat = models.ForeignKey(Seat, on_delete=models.CASCADE)
