from rest_framework import serializers

from gutsticketsapi.models import Hall, Section, Rank, Row, Seat, Show, Reservation


class HallSerializer(serializers.HyperlinkedModelSerializer):
    sections = serializers.HyperlinkedRelatedField(
        view_name="sections",
        lookup_field="sections",
        many=True,
        read_only=True
    )

    class Meta:
        model = Hall
        fields = ["name", "sections"]
