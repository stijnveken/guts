from django.apps import AppConfig


class GutsticketsapiConfig(AppConfig):
    name = 'gutsticketsapi'
