from django.contrib import admin
from gutsticketsapi.models import Reservation, Row, Show, Section, Rank, Seat, Hall

# Register your models here.

admin.site.register(Hall)
admin.site.register(Section)
admin.site.register(Rank)
admin.site.register(Row)
admin.site.register(Seat)
admin.site.register(Show)
admin.site.register(Reservation)
