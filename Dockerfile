FROM python:3.9-buster

RUN useradd -u 1000 pythonuser

WORKDIR /usr/src/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .
WORKDIR ./gutstickets

USER pythonuser
