# GUTS Technical Challenge

## Exercise 1
My aim was to go for an OOP approach. I divided a hall into
its component parts and made classes for each. Right now
they only have a limited functionality and are purely for holding
data. The idea is that there is 1 Hall, which holds multiple sections.
A section will hold multiple ranks. A rank holds multiple rows.
And finally a row will hold multiple seats.

## Exercise 2
For exercise two the obvious problem was filling the rows from left to
right for even numbered rows, and from right to left for uneven
numbered rows. I solved this by having a separate function that calculates
the current coordinate. This only works because we can assume that
the number of guests is lower or equal to the amount of seats.
Otherwise it would overflow and result in an IndexError.

## Exercise 3
This one was more difficult than I expected. First I tried a naive approach
where if a group didn't fit it would land in a queue and was the first to be
tried again after another group was seated. This worked but would get stuck
with the larger groups at the end which won't fit together.

Next I tried to solve it with backtracking. I am convinced this is the
solution but I've never implemented this algorithm before and couldn't
figure it out for this use case given the amount of time I had. You'll find
an incomplete and erroring solution unfortunately.

I decided to abandon this exercise in favor of getting as far as possible
with the others. 

## Exercise 4
For the django models I followed the classes I made in exercise 1. I added
a Show class and a Reservation class which can be used to store the seating
per event.

## Exercise 5
My idea was to use django-restframework to make the rest api. I've used this
in the past. Unfortunately I ran out of time, I've spend around 8 hours on 
the exercises so far and that was the time I allotted for this. I tried to
show my thought process by creating this readme. One thing I got turned around on
is the database relationships in combination with django-restframework.

Should you want to try to run the code, there is a Dockerfile and docker-compose
provided.
