from typing import List, Tuple

from guts.exercise1 import Rank, Seat, Row


def guts_rank() -> Rank:
    rank = Rank("guts")
    # Like in the example I want a rank of 3 rows with 8 seats
    rows: List[Row] = []
    for _ in range(0, 3):
        seats = [Seat(number) for number in range(1, 9)]
        row = Row()
        row.seats = seats
        rows.append(row)
    rank.rows = rows
    return rank


def fill_rank(rank: Rank, users: List[int]):
    def seat_user(rank: Rank, y: int, x: int, user: int):
        rank.rows[y].seats[x].occupant = user

    def update_seating_cords(rank: Rank, y: int, x: int) -> Tuple[int, int]:
        """get the next cords of a free chair

        uneven rows fill from left to right, even rows fill from right to left
        """
        # minus 1 to avoid IndexError
        max_x = len(rank.rows[0].seats) - 1
        if y % 2 == 0:
            if x == max_x:
                # The next row is uneven so it will count down.
                # No need to reset the x cord
                return y + 1, x
            else:
                return y, x + 1
        else:
            if x == 0:
                return y + 1, x
            else:
                return y, x - 1

    # Assuming none of the seats are filled and the rank is a rectangle
    y = x = 0
    for group_number, group in enumerate(users, 1):
        for _ in range(0, group):
            seat_user(rank, y, x, group_number)
            y, x, = update_seating_cords(rank, y, x)


def print_rank(rank: Rank):
    for row in rank.rows:
        print([seat.occupant for seat in row.seats], sep=",")


def exercise2():
    """Here we fill a rank with users"""
    rank = guts_rank()
    # taken from the assignment
    users = [1, 3, 4, 4, 5, 1, 2, 4]
    fill_rank(rank, users)
    print_rank(rank)
