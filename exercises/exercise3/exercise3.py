from collections import namedtuple
from typing import List, Tuple

from guts.exercise1 import Rank, Seat, Row

Group = namedtuple("Group", "group_number amount")


def guts_rank() -> Rank:
    rank = Rank("guts")
    # Like in the example I want a rank of 3 rows with 8 seats
    rows: List[Row] = []
    for _ in range(0, 3):
        seats = [Seat(number) for number in range(1, 9)]
        row = Row()
        row.seats = seats
        rows.append(row)
    rank.rows = rows
    return rank


def fill_rank(rank: Rank, groups: List[Group]):
    def seat_user(rank: Rank, y: int, x: int, user: int):
        rank.rows[y].seats[x].occupant = user

    def seating_cords(rank) -> Tuple[int, int]:
        for index_y, row in enumerate(rank.rows):
            seats = row.seats if index_y % 2 == 0 else reversed(row.seats)
            for index_x, seat in enumerate(seats):
                if seat.occupant is None:
                    if index_y % 2 == 0:
                        return index_y, index_x
                    else:
                        return index_y, len(rank.rows[index_y].seats) - index_y

    def update_seating_cords(rank: Rank, y: int, x: int) -> Tuple[int, int]:
        """get the next cords of a free chair

        uneven rows fill from left to right, even rows fill from right to left
        """
        # minus 1 to avoid IndexError
        max_x = len(rank.rows[0].seats) - 1
        if y % 2 == 0:
            if x == max_x:
                # The next row is uneven so it will count down.
                # No need to reset the x cord
                return y + 1, x
            else:
                return y, x + 1
        else:
            if x == 0:
                return y + 1, x
            else:
                return y, x - 1

    def next_group(rank: Rank, groups: List[Group]):
        for group_index, group in enumerate(groups):
            free_seats = rank.rows[y].free_seats()

            if free_seats >= group.amount:
                return group_index, group
        return None
    # Assuming none of the seats are filled and the rank is a rectangle
    cords = seating_cords(rank)
    if cords is None:
        print_rank(rank)
        return
    y, x, = cords
    while True:
        if len(groups) == 0:
            print_rank(rank)
            return

        while True:
            group = next_group(rank, groups)
            if group is None:
                break
            group_index, group = group

            for _ in range(0, group.amount):
                seat_user(rank, y, x, group.group_number)
                y, x, = update_seating_cords(rank, y, x)
            groups.pop(group_index)
            fill_rank(rank, groups)
            # now backtrack, insert the group back into the list
            # clear the seats, and fix the cords
            groups.insert(group_index, group)
            for seat in rank.rows[y].seats:
                if seat.occupant == group.group_number:
                    seat.occupant = None
            x = x - group.amount if y % 2 == 0 else x + group.amount
        return



def print_rank(rank: Rank):
    for row in rank.rows:
        print([seat.occupant for seat in row.seats], sep=",")


def exercise3():
    """Here we fill a rank with users"""
    rank = guts_rank()
    # taken from the assignment
    users = [1, 3, 4, 4, 5, 1, 2, 4]
    groups = [
        Group(group_number, amount) for group_number, amount in enumerate(users, 1)
    ]
    fill_rank(rank, groups)
    print_rank(rank)
