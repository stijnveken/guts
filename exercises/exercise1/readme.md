#Exercise 1

My aim was to go for an OOP approach. I divided a hall into
its component parts and made classes for each. Right now
they only have a limited functionality and are purely for holding
data. The idea is that there is 1 Hall, which holds multiple sections.
A section will hold multiple ranks. A rank holds multiple rows.
And finally a row will hold multiple seats.