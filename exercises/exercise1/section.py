from typing import List

from guts.exercise1 import Rank


class Section:
    """The section holds the different ranks"""

    name: str
    _ranks: List[Rank]

    def __init__(self, name: str):
        self.name = name

    @property
    def ranks(self) -> List[Rank]:
        return self.ranks

    @ranks.setter
    def ranks(self, value: List[Rank]):
        self._ranks = value
