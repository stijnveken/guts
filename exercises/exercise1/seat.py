class Seat:
    """The seat has a number (int) and its current occupant
    """

    number: int
    _occupant: str

    def __init__(self, number: int, occupant=None):
        self.number = number
        self.occupant = occupant

    @property
    def occupant(self):
        return self._occupant

    @occupant.setter
    def occupant(self, value: str):
        self._occupant = value

    def is_taken(self) -> bool:
        return self.occupant is not None
