from typing import List

from guts.exercise1 import Section


class Hall:
    """The hall is the overarching datastructure, it holds all the different sections"""
    name: str
    _sections: List[Section]

    def __init__(self, name: str):
        self.name = name

    @property
    def sections(self) -> List[Section]:
        return self._sections

    @sections.setter
    def sections(self, value: List[Section]):
        self._sections = value
