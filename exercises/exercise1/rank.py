from typing import List

from guts.exercise1 import Row


class Rank:
    """A rank will have a name and a collection of rows.

    The amount of rows could be configurable in the future but like with the amount
    of seats in the rows, I'm sticking to the example
    """

    name: str
    _rows: List[Row]

    def __init__(self, name: str):
        self.name = name

    @property
    def rows(self) -> List[Row]:
        return self._rows

    @rows.setter
    def rows(self, value: List[Row]):
        self._rows = value
