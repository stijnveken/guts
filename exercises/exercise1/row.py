from typing import List

from guts.exercise1 import Seat


class Row:
    """A row holds a collection of seats"""

    _seats: List[Seat]

    @property
    def seats(self) -> List[Seat]:
        return self._seats

    @seats.setter
    def seats(self, value: List[Seat]):
        self._seats = value

    def free_seats(self) -> int:
        if self.seats[0].occupant is None and self.seats[-1].occupant is None:
            # entire row is empty, doesn't matter from which way we fill it
            return len(self.seats)
        elif self.seats[0].occupant is not None or self.seats[-1].occupant is not None:
            # the row is being filled from left to right
            return len(list(filter(lambda x: x.occupant is None, self.seats)))
        else:
            # the row is full
            return 0
